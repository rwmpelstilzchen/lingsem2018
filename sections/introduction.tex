\begin{frame}{What is this talk about?}
	\begin{itemize}
		\item Two demonstrative:
			\begin{itemize}
				\item {\seformsfullgl} (‘\se’)
				\item {\thesformsfullgl} (‘\ðis’)
			\end{itemize}
		\item We will survey their functional distribution and structural features.
	\end{itemize}
\end{frame}


\begin{frame}{Orthographic notes}
	\begin{itemize}
		\item \OE{Þ/þ} and \OE{Ð/ð} are in free variation; phonetic value: [{\fontspec{Gentium}θ/ð}]
		\item Macron signifies a following nasal (usually \OE{-m})\\
			Hence,
			\OE{þam}~=
			\OE{ðam}~=
			\OE{þā}~=
			\OE{ðā}
		\item Scribal abbreviations:
			\begin{itemize}
				\item \OE{}~= \OE{þæt} ‘that’
				\item \OE{⁊}~= \OE{and} ‘and’
			\end{itemize}
	\end{itemize}
\end{frame}


\begin{frame}{What do I want you to take from this talk?}
	\begin{enumerate}
		\item Usefulness to your languages
		\item Data can be surprising
		\item Homonyms should not be mixed
		\item Open research
			\pause
		\item Study Old English!
	\end{enumerate}
\end{frame}


\begin{frame}{Who am I?}
	\begin{columns}[T]
		\begin{column}{.5\textwidth}
			Present (PhD):
			\vspace{1\normalbaselineskip}

			\small%
			\emph{Grammar of Textuality}: Structural Linguistic Analysis of Text Typology in Modern Welsh
		\end{column}
		\begin{column}{.5\textwidth}
			Past (MA):
			\vspace{1\normalbaselineskip}

			\small%
			\emph{se ðe ís soð wysdom~· ⁊ sawla líf}~—
			A Textual-Structural Study of the Demonstrative Pronouns in Old English:
			Classification, characterization and description
			of the examples in Ælfric’s \emph{Lives of Saints}
		\end{column}
	\end{columns}
	\begin{center}
		\includegraphics[height=0.33\textheight]{images/britain.eps}
	\end{center}
\end{frame}

\begin{frame}{MA thesis (2015)}
	\begin{columns}[t]
		\column{0.45\textwidth}
		\includegraphics[width=\textwidth, page=93]{images/mathesis.pdf}
		\column{0.45\textwidth}
		\includegraphics[width=\textwidth, page=1]{images/mathesis.pdf}
	\end{columns}
\end{frame}

\begin{frame}<1-4>[label=talk-structure]{Talk structure}
	\begin{center}
		\tikz[small mindmap, concept color=gray!30]
		\node [concept, fill=black!2, line width=5pt] {talk}
		child[concept color=myblue, grow=215] {
			node[concept] {introduction}
			child[visible on=<2->] {node[concept] {topic}}
			child[visible on=<2->] {node[concept] {methodology}}
			child[visible on=<2->] {node[concept] {corpus}}
		}
		child[concept color=mygreen, grow=270]{
			node[concept] {demonstratives}
			child[visible on=<3->] {node[concept] {\se}}
			child[visible on=<3->] {node[concept] {\ðis}}
			child[visible on=<3->] {node[concept] {\se:\ðis}}
		}
		child[concept color=myviolet, grow=-35]{
			node[concept] {open research}
			child[visible on=<4->] {node[concept] {what does it mean?}}
			child[visible on=<4->] {node[concept] {why is it good?}}
			child[visible on=<4->] {node[concept] {(how to do it?)}}
		};
	\end{center}
\end{frame}


\part{Introduction}
\frame{\partpage}

\section{Topic}

\def\Stack#1{\tabular[t]{@{}l@{}}#1\endtabular}
\begin{frame}{{\se} and {\ðis}: two demonstrative pronouns}
	\begin{tabular}{lll}
                  & \se & \ðis\\
		\midrule
		etymology & PIE *\foreign{so}, *\foreign{seh₂}, *\foreign{tod} & +NWGmc *\foreign{-s(s)-}\\
		cognates  & \Stack{Skt \Sanskrit{सः, सा, तत्} (\foreign{sáḥ}, \foreign{sā́}, \foreign{tát})\\Grk {ὁ, ἡ, τό} (\foreign{ho}, \foreign{hē}, \foreign{tó})} & \Stack{ON \foreign{þesse}, \foreign{þessi}, \foreign{þetta}\\OFrs \foreign{this}, \foreign{thius}, \foreign{thit}}\\
		OldEng    & \OE{se}, \OE{seo}, \OE{þæt} & \OE{þes}, \OE{þeos}, \OE{þis}\\
		ModEng    & \Stack{\foreign{the} (< ME \foreign{þe})\\\foreign{that} (< ME \foreign{þat})} & \Stack{\foreign{this} (< ME \foreign{þis})\\\foreign{these} (< ME \foreign{þæs})\\\foreign{those} (< ME \foreign{þas})}
	\end{tabular}
\end{frame}


\begin{frame}{Two distinct syntactic patterns}
	\begin{itemize}
		\item Dependent (determiner): [{\se}/{\ðis}~+ \textsc{noun}]
			\begin{itemize}
				\item agreement in gender, number and case
				\item weak adjectives
				\item nucleus and satellite 
				\item high juncture
			\end{itemize}
		\item<alert@2> Independent: [{\se}/{\ðis}]
	\end{itemize}

	%We deal with the \emph{independent} demonstrative pronouns only.
\end{frame}


\begin{frame}{\ex dependent {\se} (anaphora)}
	\myexample{32ead}{56}{}{1}
	{
		Hwæt þa eadmund clypode \hlminor{ænne bisceop}~·\\
		þe him þa gehendost wæs ⁊ wið hine smeade\\
		hu he þā reþan hinguare and-wyrdan sceolde~·
		\mspar
		Þa forhtode \dhl{se}{n}{m}{s} \hlA{bisceop} for þā færlican gelimpe~·\\
		⁊ for þæs cynincges life~· ⁊ cw̄  him rǽd þuhte\\
		 he to þam gebuge þe hī bead hinguar~·
	}
	{
		So then king Edmund called \hlminor{a bishop}\\
		who was handiest to him, and consulted with him\\
		how he should answer the savage Hingwar.
		\mspar
		Then \tdhl{the} \hlA{bishop} feared for this terrible misfortune,\\
		and for the king’s life, and said that it seemed best to him\\
		that he should submit to that which Hingwar bade him.
	}
\end{frame}


\begin{frame}{\ex dependent {\ðis} (\foreign{nynégocentrique} deixis)}
	\myexample{01chr}{1}{}{}
	{
		\textsc{Men ða leofestan hwilon \'ær we sædon eow hú ure} hælend crist on \dhl{þisum}{d}{m}{s} \hlA{dæge} on soðre menniscnysse acenned wæs of þæm halgan mædene marian.
	}
	{
		Men beloved, we told you erewhile how our Saviour Christ was on \tdhl{this} \hlA{day} born in true human nature of the holy Virgin Mary.
	}
\end{frame}


%\begin{frame}{\ex independent {\se} (pronominal antecedent)}
%	\myexample{32ead}{148}{}{}
%	{
%		Hi eodon þa secende~· ⁊ symle clypigende~·\\
%		swa swa hit gewunelic is \dhl{þā}{d}{}{p} \thehl{ðe} on wuda gað oft~·\\
%		hwær eart þu nu gefera~· ⁊ him ⁊wyrde  heafod~·
%		\mspar
%		Hér~· hér~· hér~· ⁊ swa gelome clypode […]
%	}
%	{
%		They went on seeking and always crying out,\\
%		as is often the wont of \tdhl{those} \thehl{who} go through woods;\\
%		‘Where art thou now, comrade?’ And the head answered them,
%		\mspar
%		‘Here, here, here.’ […]
%	}
%\end{frame}


\begin{frame}{\ex independent {\se} (introduction of new characters)}
	\myexample{02eug}{326}{}{}
	{
		Þa wǽs on rome byrig \hlB{sum cyne-boren mæden}~·\\
		\hlA{basilla} \hlC{gehaten}~· on hæðen-scype wunigende~·\\
		\dhl{seo}{n}{f}{s} wolde gehyran þá halgan lare~·\\
		of eugenian muþe~· ác heo ne mihte hire genealecean~·\\
		forðan þe cristen-dóm wǽs~· þær ónscunigend-lic~·
	}
	{
		Then was there in Rome \hlB{a maiden of royal birth},\\
		\hlC{named} \hlA{Basilla}, living in heathendom;\\
		\tdhl{she (this one)} desired to hear the holy lore\\
		at the mouth of Eugenia, but she could not approach her,\\
		because Christianity was there held in abhorrence.
	}
\end{frame}


\begin{frame}{\ex independent {\ðis} (refers to the things said)}
	\myexample{15mar}{68}{}{}
	{
		Þa astrehte se halga his handa ⁊ cw̄~·
		\mspar
		\hlA{\ellipsis}
		\mspar
		Mid þam ðe he \dhl{þis}{a}{n}{s} cwæð~· þa com crist sylf him to~·\\
		{\ellipsis}
	}
	{
		Then the Saint stretched out his hands and said,
		\mspar
		\hlA{‘{\ellipsis}’}
		\mspar
		While he was saying \tdhl{this}, there came Christ Himself to him,\\
		{\ellipsis}
	}
\end{frame}


\begin{frame}{Differences between the two}
	\begin{itemize}
		\item Internal:
			\begin{itemize}
				\item different micro- and macro-syntax
				\item different functions
				\item quantitative difference
			\end{itemize}
			\symbolglyph{⇒} should be considered homonyms ({\se}₁, {\se}₂, {\ðis}₁, {\ðis}₂)
		\item External:
			\begin{itemize}
				\item scholarly attention
			\end{itemize}
	\end{itemize}
\end{frame}


\newcommand{\deponly}[1]{\textcolor{lightgray}{#1}}
\begin{frame}[label=inflection]{Inflection}
	\scriptsize
	\begin{columns}[T]
		\column{0.5\textwidth}
		\begin{tabular}{ccccc}
			\multicolumn{5}{c}{{\se}}\\
			\toprule
			case & \multicolumn{3}{c}{singular} & plural\\
			\cmidrule(r){2-4}
			& \gram{m} & \gram{f} & \gram{n} &\\
			\midrule
			\textsc{nom} & \OE{se}   & \OE{seo}            & \OE{þæt} & \OE{þa}\\
			\textsc{acc} & \OE{þone} & \OE{þa}             & \OE{þæt} & \OE{þa}\\
			\textsc{gen} & \OE{þæs}  & \deponly{\OE{þære}} & \OE{þæs} & \OE{þæra}\\
			\textsc{dat} & \OE{þam}  & \OE{þære}           & \OE{þam} & \OE{þam}\\
			%\midrule
			%Y\\
			\bottomrule
		\end{tabular}
		\column{0.5\textwidth}
		\begin{tabular}{ccccc}
			\multicolumn{5}{c}{{\ðis}}\\
			\toprule
			case & \multicolumn{3}{c}{singular} & plural\\
			\cmidrule(r){2-4}
			& \gram{m} & \gram{f} & \gram{n} &\\
			\midrule
			\textsc{nom} & \deponly{\OE{þes}} & \deponly{\OE{þeos}}    & \OE{þis}   & \OE{þas}\\
			\textsc{acc} & \OE{þisne}         & \deponly{\OE{þas}}     & \OE{þis}   & \OE{þa}\\
			\textsc{gen} & \OE{þises}         & \deponly{\OE{þyssere}} & \OE{þises} & \deponly{\OE{þyssa}}\\
			\textsc{dat} & \OE{þysum}         & \deponly{\OE{þyssere}} & \OE{þysum} & \OE{þysum}\\
			%\midrule
			%Y\\
			\bottomrule
		\end{tabular}
	\end{columns}

	\vfill

	\textsc{inst} forms (synchronically unrelated to \se): \OE{þe}, \OE{þy}, \OE{þan}
\end{frame}



\section{Methodology}

\begin{frame}[label=selectability]{Genuine opposition and selectability}
	\ellipsis\ So we isolated the cases of neutralization and archi-elements first, in order to be able to effectively examine the functions of the forms under scrutiny in conditions of genuine opposition and selectability. \ellipsis% \small We may state with quite some satisfaction that this procedure, the preliminary isolation of neutralization environments prior to engaging in any attempt at uncovering functions, and especially on the levels superior to phonology, has become standard procedure in our school, {\ellipsis}

	%\longsource{\fullcite[46--47]{rosen.h:2005:jerusalem}}
	\source{\textcite{rosen.h:2005:jerusalem}}
\end{frame}


\begin{frame}{Textematic differences}
	\emph{Texteme}: a subtextual, componental, signalled and bounded unit manifesting a distinctive idiosyncratic and complete system of grammar; or an emic meta-textual type manifesting a distinctive idiosyncratic and complete system of grammar. The \emph{dialogue} and the \emph{narrative} are among the basic overhead textemes, further refinable and combinable into numerous others; \emph{exposition} is another (in some respects manifesting affinities with the dialogue).

	%\longsource{\fullcite[236]{shisha-halevy.a:1998:roberts}}
	\source{\textcite{shisha-halevy.a:1998:roberts}}
\end{frame}


\begin{frame}<1>[label=process]{Process}
	\begin{center}
		\begin{tikzpicture}
			\begin{scope}[every node/.style={shape=rectangle, minimum height=1cm, minimum width=5cm, node distance=0.5cm}]
				\node (a) [fill=myviolet] {corpus};
				\node (b) [fill=myblue, below=of a] {collecting examples};
				\node (c) [fill=mygreen, below=of b] {(re-)analysis\quad\symbolglyph{⟲}};
				\node (d) [fill=myred, below=of c] {presentation};
			\end{scope}
			\node [yshift=0.75cm, left=of a, rotate=-90, minimum height=6.25cm, anchor=west, fill=gray!33, single arrow, single arrow head extend=0cm, minimum width=0.75cm, draw=gray] {};
			\node [left=0.8cm of a, circle, draw, fill=myviolet] {};
			\node [left=0.8cm of b, circle, draw, fill=myblue] {};
			\node [left=0.8cm of c, circle, draw, fill=mygreen] {};
			\node [left=0.8cm of d, circle, draw, fill=myred] {};
			\node [left=1.5cm of a, align=justify, rotate=45] {\footnotesize unprocessed};
			\node [left=1.5cm of d, rotate=45] {\footnotesize processed};
			\begin{scope}[visible on=<2>]
				\begin{scope}[rounded corners]
					\node (a-tool) [right=0.5cm of a, fill=myviolet!50] {raw text file};
					\node (b-tool) [right=0.5cm of b, fill=myblue!50] {XML};
					\node (c-tool) [right=0.5cm of c, fill=mygreen!50] {SQL~+ XML};
					\node (d-tool) [right=0.5cm of d, fill=myred!50] {\LaTeX};
				\end{scope}
				\draw [->] (b-tool) -- node[right]{\footnotesize Python} (b-tool |- c-tool.north);
				\draw [<-] (d-tool) -- node[right]{\footnotesize Python} (d-tool |- c-tool.south);
			\end{scope}
		\end{tikzpicture}
	\end{center}
\end{frame}


\begin{frame}<1>[label=advantages]{Providing a full, annotated and hyper-linked database}
	\begin{center}
		\begin{tikzpicture}
			\graph[clockwise=6, radius=3cm,
				nodes={rectangle, draw, thick}] {
				a [as=transparency, onslide=<3>{fill=myblue}, onslide=<4->{fill=mygreen}],
				b [as=non-linearity, onslide=<4>{fill=myblue}, onslide=<5->{fill=mygreen}],
				c [as=high resolution, onslide=<5>{fill=myblue}, onslide=<6->{fill=mygreen}],
				d [as=quantitativeness, onslide=<6>{fill=myblue}, onslide=<7->{fill=mygreen}],
				e [as=open research, onslide=<7>{fill=myblue}, onslide=<8->{fill=mygreen}],
				f [as=cognitive biases, onslide=<8>{fill=myblue}, onslide=<9->{fill=mygreen}];
				\foreach \x in {a,...,f} {
					\foreach \y in {\x,...,f} {
						\x -- \y;
					};
				};
			};
		\end{tikzpicture}
	\end{center}

	\frametitle<2>{Six advantages}
	\frametitle<3>{Six advantages: transparency}
	\frametitle<4>{Six advantages: non-linearity}
	\frametitle<5>{Six advantages: high resolution}
	\frametitle<6>{Six advantages: quantitativeness}
	\frametitle<7>{Six advantages: open research}
	\frametitle<8>{Six advantages: cognitive biases}
\end{frame}



\section{Corpus and author}

\begin{frame}{Ælfric of Eynsham}
	\includegraphics[width=\textwidth]{images/ælfric.jpg}

	\intextsource{\href{https://www.bl.uk/manuscripts/FullDisplay.aspx?ref=Cotton_MS_Julius_E_VII&index=0}{Cotton MS Julius E VII, f.~3v}}

	\begin{itemize}
		\item c.~955~– c.~1010
		\item Benedictine monk, abbot, scholar, writer, translator
		\item The most prolific writer in Old English
		\item West-Saxon dialect
	\end{itemize}
\end{frame}

\begin{frame}{Ælfric’s Grammar and Glossary}
	\includegraphics[width=\textwidth]{images/grammar-cropped.jpg}

	%\begin{center}
	%	masculinum~· hic ðes~· femininū hec ðeos~· neutrum~· hoc ðis~·
	%\end{center}

	\includegraphics[width=\textwidth]{images/glossary.jpg}

	\source{\href{http://image.ox.ac.uk/show?collection=stj&manuscript=ms154}{Oxford, St.~John Collage, MS.~154}, f.~47v and f.~153r}

	Main sources: Priscianus (\nth{6} century), Aelius Donatus (\nth{4} century) and Isidore of Seville (c.\ 570~— 636).
	%{\small
	%	\begin{itemize}
	%		\item Priscianus (\nth{6} century): \foreign{Institutiones Grammaticæ}
	%		\item Aelius Donatus (\nth{4} century): \foreign{Ars Minor} and \foreign{Ars Maior}
	%		\item (Isidore of Seville (c.\ 570~— 636): \foreign{Etymologiæ}~› \foreign{De Grammatica})
	%	\end{itemize}
	%}
\end{frame}


\begin{frame}{Ælfric’s Colloquy}
	\begin{center}
		\includegraphics[width=0.7\textwidth, clip, trim=325px 630px 130px 940px]{images/colloquy.jpg}
	\end{center}

	\begin{description}
		\scriptsize
		\item [Teacher:] What work do you do?
		\item [Student:] I am a monk by profession, and I sing every day seven times with the brothers, but nevertheless I wish, in between, to learn to speak Latin.
		\item [Teacher:] What do you companions know (to do)?
		\item [Student:] Some are ploughmen, some shepherds, some oxherds, some also hunters, fishers, fowlers, merchants, shoemakers, salters and bakers.
		\item [Teacher:] What do you say, ploughman? How do you do your work?
	\end{description}

	\source{\href{https://bl.uk/learning/timeline/item126530.html}{Cotton MS Tiberius A III, f.~60v}}
\end{frame}


\begin{frame}{Old English Hexateuch}
	\begin{center}
		\includegraphics[height=0.8\textheight]{images/hexateuch.jpg}
	\end{center}

	\source{\href{https://commons.wikimedia.org/wiki/File:Meister_der_Paraphrasen_des_Pentateuch_001.jpg}{Cotton MS Claudius B IV, f.~19r}}
\end{frame}


\begin{frame}{De Temporibus Anni}
	\includegraphics[width=\textwidth]{images/temporibus.jpg}

	\source{\href{https://www.bl.uk/manuscripts/FullDisplay.aspx?ref=Cotton_MS_Caligula_A_XV}{Cotton MS Caligula A XV}, f.~151r}
\end{frame}


\begin{frame}{Sermones Catholici}
	\includegraphics[width=\textwidth]{images/catholici.jpg}

	\source{\href{https://emilynicolet.wordpress.com/2017/07/13/my-visit-to-the-bodleian-library/}{Oxford, Bodleian Library, MS Bodley 340}}
\end{frame}


\begin{frame}{Lives of Saints}
	\begin{columns}[T]
		\column{0.5\textwidth}
		\begin{itemize}
			\item ±40 texts, of two main genres:
				\begin{itemize}
					\item Saints’ Lives and Passions
					\item Sermons
				\end{itemize}
			\item Cotton MS Julius E VII 
			\item ±400 pp.\ in print;\\
				241 fol.\ in MS%. ±Beowulf×4
			\item Rich and idiomatic Old English adaption of Latin \foreign{vitae sanctorum}
			\item Very loosely alliterative rhythmical prose
		\end{itemize}

		\column{0.5\textwidth}
		\includegraphics[width=\textwidth]{images/aels.jpg}

		\intextsource{\href{https://www.bl.uk/manuscripts/FullDisplay.aspx?ref=Cotton_MS_Julius_E_VII&index=0}{Cotton MS Julius E VII, f.~3v}}
	\end{columns}
\end{frame}
