all:
	-mkdir --parents /tmp/latexmk/lingsem2018
	latexmk -lualatex -shell-escape -file-line-error -outdir=build/ presentation

pvc:
	-mkdir --parents /tmp/latexmk/lingsem2018
	latexmk -lualatex -shell-escape -silent -pvc -file-line-error -outdir=build/ presentation

clean:
	-rm *.aux *.bbl *.blg *.log *.toc *.url *.cut *.run.xml *.bst *.bcf *.fls *.fdb_latexmk *.out *.dvi *.idx *.ilg *.ind *.nav *.snm *.vrb *.xdv *.latexmain

distclean: clean
	-rm *.pdf
